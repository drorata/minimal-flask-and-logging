from setuptools import setup

setup(name='foo',
      version='POC',
      author='Dror Atariah',
      author_email='drorata@gmail.com',
      packages=['foo'],
      zip_safe=False)
