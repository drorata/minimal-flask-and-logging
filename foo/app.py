from flask import Flask
import logging
from foo import mod

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
formatter = logging.Formatter(
    '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)
logger.addHandler(ch)

app = Flask(__name__)


@app.route('/predict', methods=['GET'])
def predict():
    logger.info("Logging from app.py")
    # logging.info("1984")
    mod.main()
    return ('', 204)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
