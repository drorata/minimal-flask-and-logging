* Start an environment as you with and install the requirements
* Try to run `python foo/mod.py` and witness the logging.
* Start the server `python foo/app.py` and from another console run `curl localhost:5000/predict`
* Uncomment line #20 in `app.py` and run the last two commands again. Notice the wrong logging.
